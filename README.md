# Mine Sweeper

This Todo List created using HTML, Javascript, CSS and Typescript. This is a simple todo list application where user can add, delete and update todo list.


## [Live Application](https://todo-list-typescript-jade.vercel.app/)

![App Screenshot 1](public/1.png)
![App Screenshot 2](public/2.png)
![App Screenshot 3](public/3.png)

## Features

- User can add to todo list
- User can delete todo list
- User can check complete/incomplete todo list

## Tech Stack
- HTML
- Javascript
- CSS
- Typescript

**Hosting:**
- Vercel


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/todo-list-typescript.git
```

Install dependencies

```bash
  npm install
```